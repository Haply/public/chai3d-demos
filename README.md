# Chai3d For Inverse3

Chai3d demos usable with Haply's Inverse3 devices and handles.

For more details about [chai3d](chai3d.org) refer to the original
[README](readme.txt).

- [Build](#build)
- [Demos](#demos)
  - [GEL Demos](#gel-demos)
- [Code](#code)

# Build

The easiest method to build the demos on Windows is to compile using
[CMake](cmake.org) via Visual Studio. If you're comfortable with CMake on the
command line, you can use the following build steps:

```
C:\chai3d-new-cpp\bin\win-Win32> mkdir build
C:\chai3d-new-cpp\bin\win-Win32> cmake -B build -DCMAKE_BUILD_TYPE=Release
C:\chai3d-new-cpp\bin\win-Win32> ninja -C build
```

To build with Visual Studio, you'll want to start by downloading and installing
[Visual Studio Community](https://visualstudio.microsoft.com/); not be confused
with Visual Studio Code which doesn't natively come with the build toolchain
required for chai3d. Additionally, you should make sure that CMake is installed
during the installation process:

![Visual Studio Installer for CMake](doc/vs-install-cmake.png)

Once installed, you'll want to open the chai3d project
using the `CMakelists.txt` file included at the root of this repository.

![Visual Studio Open CMake Project](doc/vs-open-cmake.png)

Then switch the build to release mode to get the best performance out of the
demos.

![Visual Studio Release Mode Selection](doc/vs-build-release.png)

After CMake has had a chance to generate the necessary files, you can start the
full build using the `CTRL-SHIFT-B` shortcut or by using the `Build All` option
in the menu.

![Visual Studio Build All](doc/vs-build-all.png)

The build process in release mode can take a few minutes and, once completed, a
series of executables should now be available in the `bin/win-Win32` folder at
the root of the repository:

```
C:\chai3d-new-cpp\bin\win-Win32> dir

11/15/2022  10:30 AM    <DIR>          .
11/08/2022  12:41 PM    <DIR>          ..
11/15/2022  10:30 AM         4,019,859 01-mydevice.exe
11/15/2022  10:30 AM         4,020,141 02-multi-devices.exe
11/15/2022  10:30 AM         4,045,843 03-analytics.exe
11/15/2022  10:30 AM         4,151,772 04-shapes.exe
11/15/2022  10:30 AM         4,132,940 05-fonts.exe
...
```


# Demos

In this section, we'll go over how to configure and run the various demos using
Haply's Inverse3 devices and handles.

The first step will be to configure which COM ports will be used in the demos
using text files placed in the same folder as the examples
(i.e. `chai3d-new-cpp\bin\win-Win32`). While there is support for automatic
detection of devices and handles and their associated COM ports, the current
process can be quite slow. It's generally easier and more reliable to use the
manual method.

First we'll need to identify the COM ports used by your devices and handles. You
can do this by opening the Windows Device Manager (`Windows Button > Device
Manager`) and navigating to the `Ports (COM & LPT)` section:

![Device Manager](doc/demo-device-manager.png)

COM ports labeled as `USB Serial Device` will typically be associated to
Inverse3 devices while the `Silicon Labs CP210x USB to UART Bridge` label will
be associated to handles.

Once you've identified which COM ports you'll be using, you'll want to add them
to the `device_com_port.txt` and `tool_com_port.txt` files respecitvely where
`device_com_port.txt` will contain the COM ports associated with the devices and
`tool_com_port.txt` will contain the COM ports associated with the handles.

```
C:\chai3d-new-cpp\bin\win-Win32>type device_com_port.txt
COM15

C:\chai3d-new-cpp\bin\win-Win32>type tool_com_port.txt
COM14

```

If you're using multiple device, simply use one line per device or handles in
the configuration files. Device and handles will be matched where the first line
in each file will be be paired together and the second line in each file will be
paired, etc.

```
C:\chai3d-new-cpp\bin\win-Win32>type device_com_port.txt
COM15
COM16

```

And that's it. You may now run any of the demos to test your Inverse3 device and
handles. The following demos are good starting points for exploring the
capabilities of Haply's devices:

- `11-effect.exe`
- `14-textures.exe`
- `15-paint.exe`
- `18-endoscope.exe` (used for trying out handle orientation)


## GEL Demos

There's an additional set of GEL demos that are included as a seperate module in
chai3d. To access them, you can follow the build instructions detailed in the
[build](#build) section but using the
[CMakeLists.txt](modules/GEL/CMakeLists.txt) in the `modules/GEL` folder. The
output of the build process will be in the `modules/GEL/bin/win-Win32` folder
where you can use the configuration process detailed in the [demos](#demos)
section. Once properly built and configured, we recommend trying out the
`01-GEL-membrane.exe` demo.


# Code

The integration between the Haply Hardware C++ API and chai3d can be found in
the [`CMyCustomDevice.h`](src/devices/CMyCustomDevice.h) and it's associated
[cpp](src/devices/CMyCustomDevice.cpp) file. The API header files and binary
libraries can be found in the [`external/haply`](external/haply) folder.

You can refer to the official documentation for the Haply API for more details.

# CI/CD

One a branch is merged into the `main` branch, the following download links are updated automatically.

## Windows

[Demo bundle](http://cdn.haply.co/r/Demo-Bundle/latest/Demo-bundle.zip)

## MacOS

[OSX Archive latest](https://cdn.haply.co/r/Chai3D/latest/OSX/1.0.0.zip)
[OSX Archive 1.0.0](https://cdn.haply.co/r/Chai3D/1.0.0/OSX/1.0.0.zip)
[OSX Installer latest](https://cdn.haply.co/r/Chai3D/latest/OSX/haply-chai3d-demos.dmg)
[OSX Installer 1.0.0](https://cdn.haply.co/r/Chai3D/1.0.0/OSX/haply-chai3d-demos.dmg)

## Ubuntu

### 24.04

[Ubuntu 24.04 Archive latest](https://cdn.haply.co/r/Chai3D/latest/Ubuntu_24_04/1.0.0.zip)
[Ubuntu 24.04 Archive 1.0.0](https://cdn.haply.co/r/Chai3D/1.0.0/Ubuntu_24_04/1.0.0.zip)
[Ubuntu 24.04 Installer latest](https://cdn.haply.co/r/Chai3D/latest/Ubuntu_24_04/haply-chai3d-demos_1.0.0_amd64.deb)
[Ubuntu 24.04 Installer 1.0.0](https://cdn.haply.co/r/Chai3D/1.0.0/Ubuntu_24_04/haply-chai3d-demos_1.0.0_amd64.deb)

### 22.04

[Ubuntu 22.04 Archive latest](https://cdn.haply.co/r/Chai3D/latest/Ubuntu_22_04/1.0.0.zip)
[Ubuntu 22.04 Archive 1.0.0](https://cdn.haply.co/r/Chai3D/1.0.0/Ubuntu_22_04/1.0.0.zip)
[Ubuntu 22.04 Installer latest](https://cdn.haply.co/r/Chai3D/latest/Ubuntu_22_04/haply-chai3d-demos_1.0.0_amd64.deb)
[Ubuntu 22.04 Installer 1.0.0](https://cdn.haply.co/r/Chai3D/1.0.0/Ubuntu_22_04/haply-chai3d-demos_1.0.0_amd64.deb)

### 20.04

[Ubuntu 20.04 Archive latest](https://cdn.haply.co/r/Chai3D/latest/Ubuntu_20_04/1.0.0.zip)
[Ubuntu 20.04 Archive 1.0.0](https://cdn.haply.co/r/Chai3D/1.0.0/Ubuntu_20_04/1.0.0.zip)
[Ubuntu 20.04 Installer latest](https://cdn.haply.co/r/Chai3D/latest/Ubuntu_20_04/haply-chai3d-demos_1.0.0_amd64.deb)
[Ubuntu 20.04 Installer 1.0.0](https://cdn.haply.co/r/Chai3D/1.0.0/Ubuntu_20_04/haply-chai3d-demos_1.0.0_amd64.deb)

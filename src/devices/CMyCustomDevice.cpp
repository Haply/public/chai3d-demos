//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D.
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    \author    <http://www.chai3d.org>
    \author    Haply Co.
    \version   3.2.0 $Rev: 1869 $
*/
//==============================================================================

//------------------------------------------------------------------------------
#include "system/CGlobals.h"
#include "devices/CMyCustomDevice.h"
//------------------------------------------------------------------------------
#if defined(C_ENABLE_CUSTOM_DEVICE_SUPPORT)
//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
/*
    INSTRUCTION TO IMPLEMENT YOUR OWN CUSTOM DEVICE:

    Please review header file CMyCustomDevice.h for some initial
    guidelines about how to implement your own haptic device using this
    template.

    When ready, simply completed the next 11 documented steps described here
    below.
*/
////////////////////////////////////////////////////////////////////////////////

// Haply specific includes

#include <fstream>
#include "timers/CPrecisionClock.h"

//------------------------------------------------------------------------------
namespace chai3d
{
    //------------------------------------------------------------------------------

    namespace hAPI = Haply::HardwareAPI;

    namespace ports
    {
        static std::string inverses[256];
        static std::string handles[256];
        static std::string versegrips[256];
    }

    cMyCustomDevice::Handle::Handle(Haply::HardwareAPI::IO::SerialStream *stream) : hAPI::Devices::Handle(stream)
    {
    }

    cMyCustomDevice::Handle::~Handle()
    {
        m_exit.store(true);
        m_thread.stop();
    }

    // We run the handle in its own thread as the hardware runs more slowly then
    // the inverse3 hardware where making synchronous calls to it would slow
    // down the main thread down to the 50-60hz range.
    void cMyCustomDevice::Handle::start()
    {
        m_thread.start(run, CTHREAD_PRIORITY_GRAPHICS, this);
    }

    void cMyCustomDevice::Handle::run(void *ctx)
    {
        auto handle = reinterpret_cast<cMyCustomDevice::Handle *>(ctx);
        double start = cPrecisionClock::getCPUTimeSeconds();

        while (!handle->m_exit.load())
        {
            handle->SendHandleState(handle->m_id, 0, NULL);
            int ret = handle->Receive();

            double now = 0;
            do
            {
                now = cPrecisionClock::getCPUTimeSeconds();
            } while (now - start < (1.0 / 50));
            start = now;
        }
    }

    void cMyCustomDevice::Handle::OnReceiveHandleInfo(
        uint8_t tool_data_remaining,
        uint16_t device_id,
        uint8_t device_model_number,
        uint8_t hardware_version,
        uint8_t firmware_version)
    {
        m_id = device_id;
    }

    void cMyCustomDevice::Handle::OnReceiveHandleStatusMessage(
        uint16_t device_id,
        float *quaternion,
        uint8_t error_flag,
        uint8_t hall_effect_sensor_level,
        uint8_t user_data_length,
        uint8_t *user_data)
    {
        std::lock_guard<std::mutex> lock(m_quats_lock);

        for (int i = 0; i < 4; i++)
            m_quats[i] = quaternion[i];
        // check lenght of user data and set vector
        if (m_userData.size() < user_data_length)
        {
            for (int i = 0; i < user_data_length; i++)
                m_userData.push_back(0);
        }
        for (int i = 0; i < user_data_length; i++)
            m_userData[i] = user_data[i];
    }

    void cMyCustomDevice::Handle::OnReceiveHandleErrorResponse(
        uint16_t device_id,
        uint8_t error_code)
    {
        printf("tool error: device=%x, code=%x\n", device_id, error_code);
    }

    bool cMyCustomDevice::openLibraries()
    {
        return (C_SUCCESS);
    }

    //==============================================================================
    /*!
        Constructor of cMyCustomDevice.
    */
    //==============================================================================
    cMyCustomDevice::cMyCustomDevice(unsigned int a_deviceNumber)
    {
        // print out a message
        //  the connection to your device has not yet been established.
        m_deviceReady = false;

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 1:

            Here you should define the specifications of your device.
            These values only need to be estimates. Since haptic devices often perform
            differently depending of their configuration withing their workspace,
            simply use average values.
        */
        ////////////////////////////////////////////////////////////////////////////

        //--------------------------------------------------------------------------
        // NAME:
        //--------------------------------------------------------------------------

        // haptic device model (see file "CGenericHapticDevice.h")
        m_specifications.m_model = C_HAPTIC_DEVICE_CUSTOM;

        // name of the device manufacturer, research lab, university.
        m_specifications.m_manufacturerName = "Haply Robotics";

        // name of your device
        m_specifications.m_modelName = "Inverse3";

        //--------------------------------------------------------------------------
        // CHARACTERISTICS: (The following values must be positive or equal to zero)
        //--------------------------------------------------------------------------

        // the maximum force [N] the device can produce along the x,y,z axis.
        m_specifications.m_maxLinearForce = 10.0; // [N]

        // the maximum amount of torque your device can provide arround its
        // rotation degrees of freedom.
        m_specifications.m_maxAngularTorque = 0.2; // [N*m]

        // the maximum amount of torque which can be provided by your gripper
        m_specifications.m_maxGripperForce = 3.0; // [N]

        // the maximum closed loop linear stiffness in [N/m] along the x,y,z axis
        m_specifications.m_maxLinearStiffness = 1000.0; // [N/m]

        // the maximum amount of angular stiffness
        m_specifications.m_maxAngularStiffness = 1.0; // [N*m/Rad]

        // the maximum amount of stiffness supported by the gripper
        m_specifications.m_maxGripperLinearStiffness = 1000; // [N*m]

        // the radius of the physical workspace of the device (x,y,z axis)
        m_specifications.m_workspaceRadius = 0.15; // [m]

        // the maximum opening angle of the gripper
        m_specifications.m_gripperMaxAngleRad = cDegToRad(30.0);

        ////////////////////////////////////////////////////////////////////////////
        /*
            DAMPING PROPERTIES:

            Start with small values as damping terms can be high;y sensitive to
            the quality of your velocity signal and the spatial resolution of your
            device. Try gradually increasing the values by using example "01-devices"
            and by enabling viscosity with key command "2".
        */
        ////////////////////////////////////////////////////////////////////////////

        // Maximum recommended linear damping factor Kv
        m_specifications.m_maxLinearDamping = 6.0; // [N/(m/s)]

        //! Maximum recommended angular damping factor Kv (if actuated torques are available)
        m_specifications.m_maxAngularDamping = 0.0; // [N*m/(Rad/s)]

        //! Maximum recommended angular damping factor Kv for the force gripper. (if actuated gripper is available)
        m_specifications.m_maxGripperAngularDamping = 0.0; // [N*m/(Rad/s)]

        // Overriding the default values for the linear damping factor with the custom_conf.yml file values
        std::ifstream config("custom_conf.yml");
        if (config.is_open())
        {
            std::string line;
            while (getline(config, line))
            {
                if (line.find("maxLinearDamping") != std::string::npos)
                {
                    m_specifications.m_maxLinearDamping = std::stod(line.substr(line.find(":") + 1));
                    printf("maxLinearDamping: %f\n", m_specifications.m_maxLinearDamping);
                }
                if (line.find("maxLinearStiffness") != std::string::npos)
                {
                    m_specifications.m_maxLinearStiffness = std::stod(line.substr(line.find(":") + 1));
                    printf("maxLinearStiffness: %f\n", m_specifications.m_maxLinearStiffness);
                }
            }
            config.close();
        }

        //--------------------------------------------------------------------------
        // CHARACTERISTICS: (The following are of boolean type: (true or false)
        //--------------------------------------------------------------------------

        // does your device provide sensed position (x,y,z axis)?
        m_specifications.m_sensedPosition = true;

        // does your device provide sensed rotations (i.e stylus)?
        m_specifications.m_sensedRotation = true;

        // does your device provide a gripper which can be sensed?
        m_specifications.m_sensedGripper = false;

        // is you device actuated on the translation degrees of freedom?
        m_specifications.m_actuatedPosition = true;

        // is your device actuated on the rotation degrees of freedom?
        m_specifications.m_actuatedRotation = false;

        // is the gripper of your device actuated?
        m_specifications.m_actuatedGripper = false;

        // can the device be used with the left hand?
        m_specifications.m_leftHand = true;

        // can the device be used with the right hand?
        m_specifications.m_rightHand = true;

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 2:

            Here, you shall  implement code which tells the application if your
            device is actually connected to your computer and can be accessed.
            In practice this may be consist in checking if your I/O board
            is active or if your drivers are available.

            If your device can be accessed, set:
            m_systemAvailable = true;

            Otherwise set:
            m_systemAvailable = false;

            Your actual code may look like:

            bool result = checkIfMyDeviceIsAvailable()
            m_systemAvailable = result;

            If want to support multiple devices, using the method argument
            a_deviceNumber to know which device to setup
        */
        ////////////////////////////////////////////////////////////////////////////

        m_port = a_deviceNumber;

        m_deviceAvailable = true;
    }

    //==============================================================================
    /*!
        Destructor of cMyCustomDevice.
    */
    //==============================================================================
    cMyCustomDevice::~cMyCustomDevice()
    {
        // close connection to device
        if (m_deviceReady)
        {
            close();
        }
    }

    //==============================================================================
    /*!
        This method opens a connection to your device.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================

    bool cMyCustomDevice::openInverse()
    {
        std::string path;
        std::ifstream config("device_com_port.txt");
        if (config.is_open())
        {
            std::string line;
            for (int i = 0; i < m_port + 1; i++)
                getline(config, line);
            path = line;
            config.close();
        }
        else
            path = ports::inverses[m_port];

        printf("Connecting to device on port %s... \n", path.c_str());

        if (hAPI::isServiceRunning())
        {
            m_inverseStream.reset(new hAPI::IO::SerialStream(path.c_str(), false));
            m_inverse.reset(new hAPI::Devices::Inverse3(m_inverseStream.get()));
            printf("Haply Device Connected\n");
            return true;
        }

        m_inverseStream.reset(new hAPI::IO::SerialStream(path.c_str()));
        m_inverse.reset(new hAPI::Devices::Inverse3(m_inverseStream.get()));

        hAPI::Devices::Inverse3::DeviceInfoResponse info = m_inverse->DeviceWakeup();
        if (!info.device_model_number)
        {
            printf("No Haply Device Info Received...\n"
                   "Please check that the device is connected to the specified port\n");

            m_inverse.reset();
            m_inverseStream.reset();
            return false;
        }

        printf("Haply Device Connected\n");
        return true;
    }

    bool cMyCustomDevice::openVersegrip()
    {
        std::string path;
        std::ifstream config("versegrip_com_port.txt");
        if (config.is_open())
        {
            std::string line;
            for (int i = 0; i < m_port + 1; i++)
                getline(config, line);
            path = line;
            config.close();
        }
        else
        {
            path = ports::versegrips[m_port];
        }

        if (path.empty())
        {
            printf("No versegrip port found\n");
            return true;
        }

        printf("Connecting to versegrip on port %s... \n", path.c_str());

        if (hAPI::isServiceRunning())
        {
            m_versegripStream.reset(new hAPI::IO::SerialStream(path.c_str(), false));
            m_versegrip.reset(new hAPI::Devices::Handle(m_versegripStream.get()));
            versegrip_active = true;
            printf("Haply Versegrip Connected\n");
            return true;
        }

        m_versegripStream.reset(new hAPI::IO::SerialStream(path.c_str()));
        m_versegrip.reset(new hAPI::Devices::Handle(m_versegripStream.get()));

        hAPI::Devices::Inverse3::DeviceInfoResponse info = m_inverse->DeviceWakeup();
        versegrip_active = true;

        printf("Haply Versegrip Connected\n");
        return true;
    }

    bool cMyCustomDevice::openHandle()
    {
        std::string path;
        std::ifstream config("tool_com_port.txt");
        if (config.is_open())
        {
            std::string line;
            for (int i = 0; i < m_port + 1; i++)
                getline(config, line);
            path = line;
            config.close();
        }
        else
            path = ports::handles[m_port];

        if (path.empty())
        {
            printf("No tool port found\n");
            return true;
        }

        printf("Connecting to tool on port %s... \n", path.c_str());

        if (hAPI::isServiceRunning())
        {
            m_handleStream.reset(new hAPI::IO::SerialStream(path.c_str(), false));
            m_handle.reset(new Handle(m_handleStream.get()));

            printf("Haply Tool Connected\n");
            return true;
        }

        m_handleStream.reset(new hAPI::IO::SerialStream(path.c_str()));
        m_handle.reset(new Handle(m_handleStream.get()));

        m_handle->SendDeviceWakeup();
        int ret = m_handle->Receive();

        if (ret <= 0)
        {
            printf("No Haply Tool Info Received...\n"
                   "Please check that the tool is connected to the specified port\n");

            m_handle.reset();
            m_handleStream.reset();
            return false;
        }

        m_handle->start();

        printf("Haply Tool Connected\n");
        return true;
    }

    bool cMyCustomDevice::open()
    {

        // check if the system is available
        if (!m_deviceAvailable)
            return (C_ERROR);

        // if system is already opened then return
        if (m_deviceReady)
            return (C_ERROR);

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 3:

            Here you shall implement to open a connection to your
            device. This may include opening a connection to an interface board
            for instance or a USB port.

            If the connection succeeds, set the variable 'result' to true.
            otherwise, set the variable 'result' to false.

            Verify that your device is calibrated. If your device
            needs calibration then call method calibrate() for wich you will
            provide code in STEP 5 further below.
        */
        ////////////////////////////////////////////////////////////////////////////

        m_force.fill(0);
        m_velocity.fill(0);

        bool result = C_SUCCESS;
        result = result && openInverse();
        result = result && openHandle();
        result = result && openVersegrip();

        m_deviceReady = result;
        return result;
    }

    //==============================================================================
    /*!
        This method closes the connection to your device.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================
    bool cMyCustomDevice::close()
    {
        // check if the system has been opened previously
        if (!m_deviceReady)
            return (C_ERROR);

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 4:

            Here you shall implement code that closes the connection to your
            device.

            If the operation fails, simply set the variable 'result' to C_ERROR   .
            If the connection succeeds, set the variable 'result' to C_SUCCESS.
        */
        ////////////////////////////////////////////////////////////////////////////

        bool result = C_SUCCESS;

        m_handle.reset();
        m_handleStream.reset();

        m_inverse.reset();
        m_inverseStream.reset();

        m_deviceReady = false;
        return (result);
    }

    //==============================================================================
    /*!
        This method calibrates your device.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================
    bool cMyCustomDevice::calibrate(bool a_forceCalibration)
    {
        // check if the device is read. See step 3.
        if (!m_deviceReady)
            return (C_ERROR);

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 5:

            Here you shall implement code that handles a calibration procedure of the
            device. In practice this may include initializing the registers of the
            encoder counters for instance.

            If the device is already calibrated and  a_forceCalibration == false,
            the method may immediately return without further action.
            If a_forceCalibration == true, then the calibrartion procedure
            shall be executed even if the device has already been calibrated.

            If the calibration procedure succeeds, the method returns C_SUCCESS,
            otherwise return C_ERROR.
        */
        ////////////////////////////////////////////////////////////////////////////

        bool result = C_SUCCESS;

        return (result);
    }

    //==============================================================================
    /*!
        This method returns the number of devices available from this class of device.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================
    unsigned int cMyCustomDevice::getNumDevices()
    {
        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 6:

            Here you shall implement code that returns the number of available
            haptic devices of type "cMyCustomDevice" which are currently connected
            to your computer.

            In practice you will often have either 0 or 1 device. In which case
            the code here below is already implemented for you.

            If you have support more than 1 devices connected at the same time,
            then simply modify the code accordingly so that "numberOfDevices" takes
            the correct value.
        */
        ////////////////////////////////////////////////////////////////////////////

        {
            size_t count = 0;
            std::ifstream config("tool_com_port.txt");
            unsigned int vcount = 0;
            std::ifstream configv("versegrip_com_port.txt");
            if (config.is_open())
            {
                std::string line;
                while (getline(config, line))
                    count++;
            }
            else if (configv.is_open())
            {
                printf("Reading versegrip ports from file\n");

                std::string line;
                while (getline(configv, line))
                    vcount++;
            }
            else
            {
                std::vector<std::string> ports = hAPI::Devices::DeviceDetection::DetectHandles();
                std::vector<std::string> portsw = hAPI::Devices::DeviceDetection::DetectWiredHandles();

                count = 0;
                for (size_t i = 0; i < ports.size(); ++i)
                {
                    // if not in wired list, add to Versegrip count
                    if (std::find(portsw.begin(), portsw.end(), ports[i]) == portsw.end())
                    {
                        vcount++;
                        ports::versegrips[vcount - 1] = ports[i];
                    }
                    else
                    {
                        count++;
                        ports::handles[count - 1] = ports[i];
                    }
                }
            }
            printf("VerseGrip Count: %u \n", vcount);
            printf("Handle Count: %u \n", static_cast<unsigned int>(count));
        }

        unsigned int count = 0;
        std::ifstream config("device_com_port.txt");
        if (config.is_open())
        {
            printf("Reading inverse ports from file\n");

            std::string line;
            while (getline(config, line))
                count++;
        }
        else
        {
            std::vector<std::string> ports = hAPI::Devices::DeviceDetection::DetectInverse3s();
            count = static_cast<unsigned int>(ports.size());
            for (size_t i = 0; i < count; ++i)
            {
                ports::inverses[i] = ports[i];
            }
        }
        printf("Inverse Count: %u \n", count);

        return count;
    }

    //==============================================================================
    /*!
        This method returns the position of your device. Units are meters [m].

        \param   a_position  Return value.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================
    bool cMyCustomDevice::getPosition(cVector3d &a_position)
    {
        // check if the device is read. See step 3.
        if (!m_deviceReady)
            return (C_ERROR);

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 7:

            Here you shall implement code that reads the position (X,Y,Z) from
            your haptic device. Read the values from your device and modify
            the local variable (x,y,z) accordingly.
            If the operation fails return an C_ERROR, C_SUCCESS otherwise

            Note:
            For consistency, units must be in meters.
            If your device is located in front of you, the x-axis is pointing
            towards you (the operator). The y-axis points towards your right
            hand side and the z-axis points up towards the sky.
        */
        ////////////////////////////////////////////////////////////////////////////

        bool result = C_SUCCESS;

        hAPI::Devices::Inverse3::EndEffectorForceRequest request;
        for (size_t i = 0; i < m_force.size(); ++i)
            request.force[i] = m_force[i];

        auto response = m_inverse->EndEffectorForce(request);

        a_position.set(
            -response.position[1] - 0.150f,
            response.position[0],
            response.position[2] - 0.20f); // 0.10f

        for (size_t i = 0; i < m_velocity.size(); ++i)
            m_velocity[i] = response.velocity[i];

        if (m_handle)
        {
            if (hAPI::isServiceRunning())
            {
                m_handle->Receive();
            }
        }

        if (m_versegrip)
        {
            Haply::HardwareAPI::Devices::Handle::VersegripStatusResponse data;
            if (hAPI::isServiceRunning())
            {
                versegrip_data = m_versegrip->GetVersegripStatus();
            }
            else if (m_versegripStream->Available() >= 20)
            {
                data = m_versegrip->GetVersegripStatus();
                if (data.quaternion[0] == 0 && data.quaternion[1] == 0 &&
                    data.quaternion[2] == 0 && data.quaternion[3] == 0)
                    return C_ERROR;
                else
                {
                    versegrip_data = data;
                }
            }
        }
        return (result);
    }

    bool cMyCustomDevice::getLinearVelocity(cVector3d &a_linearvelocity)
    {
        a_linearvelocity.set(-m_velocity[1], m_velocity[0], m_velocity[2]);

        return C_SUCCESS;
    }

    //==============================================================================
    /*!
        This method returns the orientation frame of your device end-effector

        \param   a_rotation  Return value.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================
    bool cMyCustomDevice::getRotation(cMatrix3d &a_rotation)
    {
        // check if the device is read. See step 3.
        if (!m_deviceReady)
            return (C_ERROR);

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 8:

            Here you shall implement code which reads the orientation frame from
            your haptic device. The orientation frame is expressed by a 3x3
            rotation matrix. The 1st column of this matrix corresponds to the
            x-axis, the 2nd column to the y-axis and the 3rd column to the z-axis.
            The length of each column vector should be of length 1 and vectors need
            to be orthogonal to each other.

            Note:
            If your device is located in front of you, the x-axis is pointing
            towards you (the operator). The y-axis points towards your right
            hand side and the z-axis points up towards the sky.

            If your device has a stylus, make sure that you set the reference frame
            so that the x-axis corresponds to the axis of the stylus.
        */
        ////////////////////////////////////////////////////////////////////////////
        // get tool orientation

        // variables that describe the rotation matrix
        double r00, r01, r02, r10, r11, r12, r20, r21, r22;
        cMatrix3d frame;
        frame.identity();

        if (m_handle)
        {
            float Q[4] = {0, 0, 0, 0};
            {
                std::lock_guard<std::mutex> lock{m_handle->m_quats_lock};

                for (size_t i = 0; i < 4; ++i)
                    Q[i] = m_handle->m_quats[i];
            };
            Q[1] = -Q[1];
            Q[3] = -Q[3];

            r00 = 1 - 2 * (Q[1] * Q[1] + Q[3] * Q[3]);
            r01 = 2 * (Q[2] * Q[1] + Q[3] * Q[0]);
            r02 = 2 * (Q[2] * Q[3] - Q[1] * Q[0]);

            r10 = 2 * (Q[2] * Q[1] - Q[3] * Q[0]);
            r11 = 1 - 2 * (Q[2] * Q[2] + Q[3] * Q[3]);
            r12 = 2 * (Q[1] * Q[3] + Q[2] * Q[0]);

            r20 = 2 * (Q[2] * Q[3] + Q[1] * Q[0]);
            r21 = 2 * (Q[1] * Q[3] - Q[2] * Q[0]);
            r22 = 1 - 2 * (Q[1] * Q[1] + Q[2] * Q[2]);
        }
        else if (m_versegrip)
        {
            float Q[4] = {0, 0, 0, 0};

            Q[0] = versegrip_data.quaternion[0];  // w
            Q[1] = -versegrip_data.quaternion[1]; // y (used as x for correction)
            Q[2] = versegrip_data.quaternion[2];  // x (used as y for correction)
            Q[3] = -versegrip_data.quaternion[3]; // z

            r00 = 1 - 2 * (Q[1] * Q[1] + Q[3] * Q[3]);
            r01 = 2 * (Q[2] * Q[1] + Q[3] * Q[0]);
            r02 = 2 * (Q[2] * Q[3] - Q[1] * Q[0]);

            r10 = 2 * (Q[2] * Q[1] - Q[3] * Q[0]);
            r11 = 1 - 2 * (Q[2] * Q[2] + Q[3] * Q[3]);
            r12 = 2 * (Q[1] * Q[3] + Q[2] * Q[0]);

            r20 = 2 * (Q[2] * Q[3] + Q[1] * Q[0]);
            r21 = 2 * (Q[1] * Q[3] - Q[2] * Q[0]);
            r22 = 1 - 2 * (Q[1] * Q[1] + Q[2] * Q[2]);
        }
        else
        {
            // if the device does not provide any rotation capabilities
            // set the rotation matrix equal to the identity matrix.
            r00 = 1.0;
            r01 = 0.0;
            r02 = 0.0;
            r10 = 0.0;
            r11 = 1.0;
            r12 = 0.0;
            r20 = 0.0;
            r21 = 0.0;
            r22 = 1.0;
        }

        bool result = C_SUCCESS;

        frame.set(r00, r01, r02, r10, r11, r12, r20, r21, r22);

        // store new rotation matrix
        a_rotation = frame;

        // estimate angular velocity
        estimateAngularVelocity(a_rotation);

        // exit
        return (result);
    }

    //==============================================================================
    /*!
        This method returns the gripper angle in radian.

        \param   a_angle  Return value.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================
    bool cMyCustomDevice::getGripperAngleRad(double &a_angle)
    {
        // check if the device is read. See step 3.
        if (!m_deviceReady)
            return (C_ERROR);

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 9:
            Here you may implement code which reads the position angle of your
            gripper. The result must be returned in radian.

            If the operation fails return an error code such as C_ERROR for instance.
        */
        ////////////////////////////////////////////////////////////////////////////

        bool result = C_SUCCESS;

        // return gripper angle in radian
        a_angle = 0.0; // a_angle = getGripperAngleInRadianFromMyDevice();

        // estimate gripper velocity
        estimateGripperVelocity(a_angle);

        // exit
        return (result);
    }

    //==============================================================================
    /*!
        This method sends a force [N] and a torque [N*m] and gripper torque [N*m]
        to your haptic device.

        \param   a_force  Force command.
        \param   a_torque  Torque command.
        \param   a_gripperForce  Gripper force command.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================
    bool cMyCustomDevice::setForceAndTorqueAndGripperForce(const cVector3d &a_force,
                                                           const cVector3d &a_torque,
                                                           const double a_gripperForce)
    {
        // check if the device is read. See step 3.
        if (!m_deviceReady)
            return (C_ERROR);

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 10:

            Here you may implement code which sends a force (fx,fy,fz),
            torque (tx, ty, tz) and/or gripper force (gf) command to your haptic device.

            If your device does not support one of more of the force, torque and
            gripper force capabilities, you can simply ignore them.

            Note:
            For consistency, units must be in Newtons and Newton-meters
            If your device is placed in front of you, the x-axis is pointing
            towards you (the operator). The y-axis points towards your right
            hand side and the z-axis points up towards the sky.

            For instance: if the force = (1,0,0), the device should move towards
            the operator, if the force = (0,0,1), the device should move upwards.
            A torque (1,0,0) would rotate the handle counter clock-wise around the
            x-axis.
        */
        ////////////////////////////////////////////////////////////////////////////

        bool result = C_SUCCESS;

        // store new force value.
        m_prevForce = a_force;
        m_prevTorque = a_torque;
        m_prevGripperForce = a_gripperForce;

        // retrieve force, torque, and gripper force components in individual variables
        float fx = (float)a_force(0);
        float fy = (float)a_force(1);
        float fz = (float)a_force(2);

        m_force[0] = fy;
        m_force[1] = -fx;
        m_force[2] = fz;

        return (result);
    }

    //==============================================================================
    /*!
        This method returns status of all user switches
        [__true__ = __ON__ / __false__ = __OFF__].

        \param  a_userSwitches  Return the 32-bit binary mask of the device buttons.

        \return __true__ if the operation succeeds, __false__ otherwise.
    */
    //==============================================================================
    bool cMyCustomDevice::getUserSwitches(unsigned int &a_userSwitches)
    {
        // check if the device is read. See step 3.
        if (!m_deviceReady)
            return (C_ERROR);

        ////////////////////////////////////////////////////////////////////////////
        /*
            STEP 11:

            Here you shall implement code that reads the status all user switches
            on your device. For each user switch, set the associated bit on variable
            a_userSwitches. If your device only has one user switch, then set
            a_userSwitches to 1, when the user switch is engaged, and 0 otherwise.
        */
        ////////////////////////////////////////////////////////////////////////////

        // versegrip buttons if versegrip is active
        if (versegrip_active)
        {
            a_userSwitches = versegrip_data.buttons;
            return (C_SUCCESS);
        }
        else if (m_handle)
        {
            if (m_handle->m_userData.size() >= 3)
            {
                unsigned int userbuttons = 0;
                if (m_handle->m_userData[0])
                    userbuttons |= 0x01;
                if (m_handle->m_userData[1])
                    userbuttons |= 0x02;
                if (m_handle->m_userData[2])
                    userbuttons |= 0x04;

                a_userSwitches = userbuttons;
            }
            return (C_SUCCESS);
        }
        // a_userSwitches |= (versegrip_buttons & 0x01) ? 0x01 : 0;
        // a_userSwitches |= (versegrip_buttons & 0x02) ? 0x02 : 0;
        // a_userSwitches |= (versegrip_buttons & 0x04) ? 0x04 : 0;
        // a_userSwitches |= (versegrip_buttons & 0x08) ? 0x08 : 0;

        a_userSwitches = 0;
        return (C_SUCCESS);
    }

    //------------------------------------------------------------------------------
} // namespace chai3d
//------------------------------------------------------------------------------
#endif // C_ENABLE_CUSTOM_DEVICE_SUPPORT
//------------------------------------------------------------------------------

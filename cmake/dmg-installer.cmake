set(CPACK_GENERATOR "DragNDrop")

set(CPACK_PACKAGE_FILE_NAME "haply-chai3d-demos")

set(CPACK_DMG_FORMAT "UDZO")
# Setting a background image is not a trivial task.
# https://stackoverflow.com/questions/16596655/cant-configure-background-image-for-dmg-installation-using-cmake
# set(CPACK_DMG_BACKGROUND_IMAGE "${INSTALLER_FILES}/dmg/haply.png")
set(CPACK_DMG_SLA_USE_RESOURCE_FILE_LICENSE OFF) # Since it has been deprecated in OSX 12.0
set(CPACK_DMG_DISABLE_APPLICATIONS_SYMLINK OFF)

set(CPACK_COMPONENTS_ALL CHAI3D_EXAMPLES)

set(CPACK_PACKAGE_CONTACT "https://www.haply.co/connect")
set(CPACK_PACKAGE_DIRECTORY "${PROJECT_SOURCE_DIR}/cpack")
set(CPACK_PACKAGE_VERSION_MAJOR ${CMAKE_PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${CMAKE_PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${CMAKE_PROJECT_VERSION_PATCH})

include(CPack)